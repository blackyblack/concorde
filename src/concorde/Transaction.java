package concorde;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.List;

import concorde.util.Convert;
import concorde.util.Logger;

class Transaction {

  public short deadline;
  public byte[] senderPublicKey;
  public long amountNQT;
  public long feeNQT;
  public byte type;
  public byte subtype;
  public byte version;
  public int timestamp;
  //public Attachment.AbstractAttachment attachment;

  public long recipientId;
  public long senderId;
  public String referencedTransactionFullHash;
  public byte[] signature;
  public Appendix.Message message;
  public Appendix.EncryptedMessage encryptedMessage;
  public long blockId;
  public int height = Integer.MAX_VALUE;
  public long id;
  public String senderRS;
  public int blockTimestamp = -1;
  public String fullHash;
  public int ecBlockHeight;
  public long ecBlockId;
  
  public List<? extends Appendix.AbstractAppendix> appendages;
  public int appendagesSize;
  
  private int getFlags() {
    int flags = 0;
    int position = 1;
    if (message != null) {
        flags |= position;
    }
    position <<= 1;
    if (encryptedMessage != null) {
        flags |= position;
    }
    position <<= 1;
    position <<= 1;
    return flags;
}
  
  private int signatureOffset() {
    return 1 + 1 + 4 + 2 + 32 + 8 + (8 + 8 + 32);
}
  
  int getSize() {
    return signatureOffset() + 64  + (version > 0 ? 4 + 4 + 8 : 0) + appendagesSize;
  }
  
  public byte[] getBytes()
  {
    try {
        ByteBuffer buffer = ByteBuffer.allocate(getSize());
        buffer.order(ByteOrder.LITTLE_ENDIAN);
        buffer.put(type);
        buffer.put((byte) ((version << 4) | subtype));
        buffer.putInt(timestamp);
        buffer.putShort(deadline);
        buffer.put(senderPublicKey);
        buffer.putLong(recipientId);
        buffer.putLong(amountNQT);
        buffer.putLong(feeNQT);
        if (referencedTransactionFullHash != null) {
            buffer.put(Convert.parseHexString(referencedTransactionFullHash));
        } else {
            buffer.put(new byte[32]);
        }
        
        buffer.put(signature != null ? signature : new byte[64]);
        if (version > 0) {
            buffer.putInt(getFlags());
            buffer.putInt(ecBlockHeight);
            buffer.putLong(ecBlockId);
        }
        for (Appendix appendage : appendages) {
            appendage.putBytes(buffer);
        }
        return buffer.array();
    } catch (RuntimeException e) {
        Logger.logDebugMessage("Failed to get transaction bytes for transaction");
        throw e;
    }
  }
}