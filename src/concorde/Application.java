package concorde;

import java.io.Console;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.DefaultServlet;
import org.eclipse.jetty.servlet.FilterHolder;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.servlets.CrossOriginFilter;

import concorde.crypto.Crypto;
import concorde.http.APITestServlet;
import concorde.util.Convert;
import concorde.util.Logger;

public class Application
{  
  private static final Properties defaultProperties = new Properties();
  static {
      try (InputStream is = ClassLoader.getSystemResourceAsStream("concorde-default.properties")) {
          if (is != null) {
            Application.defaultProperties.load(is);
          } else {
              String configFile = System.getProperty("concorde-default.properties");
              if (configFile != null) {
                  try (InputStream fis = new FileInputStream(configFile)) {
                    Application.defaultProperties.load(fis);
                  } catch (IOException e) {
                      throw new RuntimeException("Error loading concorde-default.properties from " + configFile);
                  }
              } else {
                  throw new RuntimeException("concorde-default.properties not in classpath and system property concorde-default.properties not defined either");
              }
          }
      } catch (IOException e) {
          throw new RuntimeException("Error loading concorde-default.properties", e);
      }
  }
  private static final Properties properties = new Properties(defaultProperties);
  static {
      try (InputStream is = ClassLoader.getSystemResourceAsStream("concorde.properties")) {
          if (is != null) {
              Application.properties.load(is);
          } // ignore if missing
      } catch (IOException e) {
          throw new RuntimeException("Error loading concorde.properties", e);
      }
  }

  public static int getIntProperty(String name) {
      try {
          int result = Integer.parseInt(properties.getProperty(name));
          return result;
      } catch (NumberFormatException e) {
          return 0;
      }
  }
  
  public static double getDoubleProperty(String name) {
    try {
      double result = Double.parseDouble(properties.getProperty(name));
        return result;
    } catch (NumberFormatException e) {
        return 0;
    }
}

  public static String getStringProperty(String name) {
      return getStringProperty(name, null);
  }

  public static String getStringProperty(String name, String defaultValue) {
      String value = properties.getProperty(name);
      if (value != null && ! "".equals(value)) {
          return value;
      } else {
          return defaultValue;
      }
  }

  public static List<String> getStringListProperty(String name) {
      String value = getStringProperty(name);
      if (value == null || value.length() == 0) {
          return Collections.emptyList();
      }
      List<String> result = new ArrayList<>();
      for (String s : value.split(";")) {
          s = s.trim();
          if (s.length() > 0) {
              result.add(s);
          }
      }
      return result;
  }

  public static Boolean getBooleanProperty(String name) {
      String value = properties.getProperty(name);
      if (Boolean.TRUE.toString().equals(value)) {
          return true;
      } else if (Boolean.FALSE.toString().equals(value)) {
          return false;
      }
      return false;
  }
  
  public static final Set<String> allowedBotHosts;
  static {
    List<String> allowedBotHostsList = getStringListProperty("concorde.allowedBotHosts");
    if (! allowedBotHostsList.contains("*")) {
        allowedBotHosts = Collections.unmodifiableSet(new HashSet<>(allowedBotHostsList));
    } else {
        allowedBotHosts = null;
    }
  }
  
  public static final int port = getIntProperty("concorde.apiServerPort");
  public static final String host = getStringProperty("concorde.apiServerHost");
  
  public static final String version;
  static
  {
    if(getBooleanProperty("concorde.isTestnet"))
    {
      version = Constants.CONCORDE_VERSION + "-test";
    }
    else
    {
      version = Constants.CONCORDE_VERSION;
    }
  }
  
  public static Boolean terminated = false;
  
  public static void main(String[] args)
  {    
    String secret = Convert.emptyToNull(getStringProperty("concorde.secret", ""));
    
    if(secret == null)
    {
      Console console = System.console();
      
      if(console != null)
      {
        console.printf("%1s%n", "No secret phrase in concorde.conf found");
        console.printf("%1s", "Please enter your secret phrase: ");
        char[] secretValue = console.readPassword();
        secret = new StringBuilder().append(secretValue).toString();
      }
    }
    
    start(secret);
  }
  
  public static void start(String secret)
  {
    Logger.logMessage("Concorde " + version);
    Server apiServer = new Server();
    
    try
    {
      if(secret == null)
      {
        Logger.logWarningMessage("Cannot start Concorde. Secret phrase not specified");
        return;
      }
    
      ServerConnector connector = new ServerConnector(apiServer);
      connector.setPort(port);
      connector.setHost(host);    
      connector.setIdleTimeout(getIntProperty("concorde.apiServerIdleTimeout"));
      connector.setReuseAddress(true);
      apiServer.addConnector(connector);
      
      HandlerList apiHandlers = new HandlerList();    
      ServletContextHandler apiHandler = new ServletContextHandler();
      apiHandler.addServlet(APITestServlet.class, "/concorde");
      
      ServletHolder defaultServletHolder = new ServletHolder(new DefaultServlet());
      defaultServletHolder.setInitParameter("dirAllowed", "false");
      defaultServletHolder.setInitParameter("resourceBase", "html");
      defaultServletHolder.setInitParameter("welcomeServlets", "true");
      defaultServletHolder.setInitParameter("redirectWelcome", "true");
      apiHandler.addServlet(defaultServletHolder, "/*");
      apiHandler.setWelcomeFiles(new String[]{"index.html"});
      
      //allow CORS
      if (getBooleanProperty("concorde.apiServerCORS"))
      {
        FilterHolder filterHolder = apiHandler.addFilter(CrossOriginFilter.class, "/*", null);
        filterHolder.setInitParameter("allowedHeaders", "*");
        filterHolder.setAsyncSupported(true);
      }
  
      apiHandlers.addHandler(apiHandler);
      apiServer.setHandler(apiHandlers);
      apiServer.setStopAtShutdown(true);
      
      try
      {
        apiServer.start();
      }
      catch (Exception e)
      {
        Logger.logMessage("Could not start API server", e);
        return;
      }
      
      Logger.logMessage("Started API server at " + host + ":" + port);
      
      EscrowMain escrow = EscrowMain.instance;
      try
      {
        byte[] publicKey = Crypto.getPublicKey(secret);
        Long accountId = Convert.publicKeyToAccountId(publicKey);
        String account = Convert.rsAccount(accountId);
        
        Logger.logMessage("Concorde started on account " + account);
        escrow.run(secret);
      } 
      catch (InterruptedException e)
      {
        Logger.logWarningMessage("Concorde terminated");
      }
      catch (NxtException e)
      {
        Logger.logMessage("Concorde terminated", e);
      }
    }
    finally
    {
      try
      {
        apiServer.stop();
      }
      catch (Exception e)
      {
        Logger.logMessage("Could not stop API server", e);
      }
      
      Logger.logMessage("Concorde stopped");
    }
  }
  
  public static void stop()
  {
    Logger.logMessage("Stopping Concorde...");
    EscrowMain escrow = EscrowMain.instance;
    escrow.terminated = true;
  }
}
