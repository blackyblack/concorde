package concorde;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeMap;

import org.json.simple.JSONObject;

import timedstate.TimedFilter;
import timedstate.TimedRow;
import timedstate.TimedState;
import concorde.Appendix.EncryptedMessage;
import concorde.EscrowLock.EscrowLockStatus;
import concorde.EscrowMessage.EscrowMessageType;
import concorde.NxtException.NotValidException;
import concorde.NxtException.NxtApiException;
import concorde.crypto.Crypto;
import concorde.crypto.EncryptedData;
import concorde.state.EscrowState;
import concorde.state.EscrowStateAssetFee;
import concorde.state.EscrowStateProvider;
import concorde.util.Convert;
import concorde.util.JSON;
import concorde.util.Logger;
import concorde.util.TransactionUtils;

public class EscrowMain
{
  public static final EscrowMain instance = new EscrowMain();
  public NxtApi api = new NxtApi();

  public static final int processPeriodBlocks;
  static
  {
    processPeriodBlocks = Application.getIntProperty("concorde.processPeriodBlocks");
  }

  public static final int requestConfirmations;
  static
  {
    requestConfirmations = Application.getIntProperty("concorde.requestConfirmations");
  }

  private static Object lock = new Object();
  public boolean terminated = false;
  private TimedState db = null;

  // listings (ids) int time order
  public List<String> requestIds = new ArrayList<String>();
  
  //a blockchain stored state of the escrow service
  public EscrowStateProvider escrowState = null;

  //current block
  public Long blocks = 0L;
  //is Concorde running
  public boolean running = false;

  public TreeMap<String, EscrowLock> escrowLocks = new TreeMap<String, EscrowLock>();

  private EscrowMain()
  {
  }

  // start escrow service
  @SuppressWarnings("unchecked")
  public void run(String secret) throws NxtException, InterruptedException
  {
    boolean firstRun = true;
    Long previousRunBlocks = 0L;
    
    byte[] publicKey = Crypto.getPublicKey(secret);
    Long accountId = Convert.publicKeyToAccountId(publicKey);
    String account = Convert.rsAccount(accountId);

    TimedFilter f = new TimedFilter();
    f.dbName = "concorde";
    f.maxDbVersion = Constants.CONCORDE_REQUEST_VERSION;
    f.minDbVersion = Constants.CONCORDE_REQUEST_VERSION;
    //I do not want to skip payments
    f.minConfirmations = 1L;
    db = new TimedState(account, f);
    
    escrowState = new EscrowStateProvider(account);
    
    running = false;

    while (!terminated)
    {
      Thread.sleep(1000);

      // 1. wait until blockchain is downloaded
      if (!isBlockchainReady())
      {
        firstRun = true;
        Logger.logWarningMessage("Blockchain is not ready. Retrying in 30 sec...");
        Thread.sleep(30000);
        continue;
      }

      JSONObject status = api.getBlockchainStatus();
      if (status == null)
      {
        firstRun = true;
        Logger.logWarningMessage("Blockchain is not ready. Retrying in 30 sec...");
        Thread.sleep(30000);
        continue;
      }

      Long blocksNow = Convert.nullToZero((Long) status.get("numberOfBlocks"));
      Long blocksFeed = Convert.nullToZero((Long) status.get("lastBlockchainFeederHeight"));

      if (blocksNow <= 0 || blocksFeed <= 0)
      {
        firstRun = true;
        Logger.logWarningMessage("Blockchain is not ready. Retrying in 30 sec...");
        Thread.sleep(30000);
        continue;
      }
      
      blocks = blocksNow;

      // 2. wait for at least startupBlocks blocks
      if (firstRun)
      {
        previousRunBlocks = blocks;
        firstRun = false;
      }

      if (blocks < previousRunBlocks)
      {
        continue;
      }

      // 3. make scanRequests every 5 blocks
      if ((blocks - previousRunBlocks) < processPeriodBlocks)
      {
        continue;
      }

      Logger.logMessage("Block #" + blocks + ". Checking requests...");
      synchronized (lock)
      {
        //here we gather all requests to escrowLocks struct
        scanRequests(account, secret);
        
        // 4. make payments if necessary
        // generate new state of the locks
        for (String key : escrowLocks.keySet())
        {
          EscrowLock lockInfo = escrowLocks.get(key);
          EscrowLockStatus lockStatus = lockInfo.updateStatus(blocks);
          
          Set<EscrowLockStatus> skipStatuses = new HashSet<EscrowLockStatus>();
          
          // undefined status or closed lock
          skipStatuses.add(EscrowLockStatus.NONE);
          // already payed
          skipStatuses.add(EscrowLockStatus.PAYED_A);
          skipStatuses.add(EscrowLockStatus.PAYED_B);
          // reward gained
          skipStatuses.add(EscrowLockStatus.REWARD);
          // waiting for frozen timeout
          skipStatuses.add(EscrowLockStatus.FROZEN_A);
          skipStatuses.add(EscrowLockStatus.FROZEN_B);
          
          if(skipStatuses.contains(lockStatus))
          {
            continue;
          }
  
          if (lockStatus == EscrowLockStatus.UNLOCKED_A)
          {
            EscrowMessage message = lockInfo.requestIds.get(0);  
            if (message == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }
            
            if (message.row == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }
  
            JSONObject messageValue = message.row.parsedMessage;
            if (messageValue == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }

            boolean isAsset = false;
            String assetId = TransactionUtils.getAssetsId(message.row.transaction);
            Long assetNQT = TransactionUtils.getAssetsNQT(message.row.transaction);
            
            JSONObject unlockTransaction = null;
            if(lockInfo.unlockMessage != null && lockInfo.unlockMessage.row != null)
            {
              JSONObject unlockMessageValue = lockInfo.unlockMessage.row.parsedMessage;
              if (unlockMessageValue != null)
              {
                unlockTransaction = (JSONObject) unlockMessageValue.get("transaction");
              }
            }
            
            //extract encrypted message and send it as a separate attachment
            EncryptedData encData = null;
            try
            {
              EncryptedMessage encryptedMessage = Appendix.EncryptedMessage.parse(unlockTransaction);
              if(encryptedMessage != null)
              {
                encData = encryptedMessage.getEncryptedData();
              }
              unlockTransaction.remove("encryptedMessage");
            }
            catch(Exception e)
            {
            }
            
            JSONObject payment = new JSONObject();
            payment.put("concorde", Constants.CONCORDE_REQUEST_VERSION);
            payment.put("pay", 1L);
            payment.put("lock_id", lockInfo.lockTxId);
            JSON.putNonNull(payment, "transaction", unlockTransaction);
            
            if(assetId != null && assetNQT > 0)
            {
              isAsset = true;
            }
            
            if(isAsset)
            {
              Long newAssetNQT = assetNQT;
              EscrowStateAssetFee assetFee = lockInfo.lockState.assetFees.get(assetId);
              
              if(assetFee == null) continue;
              
              Long fee = calculateAssetFee(assetId, assetNQT, assetFee);
              newAssetNQT -= fee;
              if(newAssetNQT <= 0)
                continue;
              
              String txid = api.assetTransferSafe(lockInfo.peerB, secret, payment.toString(), encData, assetId, newAssetNQT);
              Logger.logMessage("Payment assets [" + txid + "] from peerA " + lockInfo.peerA + " to peerB " + lockInfo.peerB +
                  ". Asset = " + assetId +
                  ". Amount = " + newAssetNQT);
              continue;
            }
  
            Long newPayment = lockInfo.paymentNQT;
            Long fee = (long) (newPayment * lockInfo.lockState.escrowFee);
            if (fee < lockInfo.lockState.minEscrowFee)
              fee = (long) lockInfo.lockState.minEscrowFee;
  
            newPayment -= fee;
  
            String txid = api.pay(lockInfo.peerB, secret, newPayment, payment.toString(), encData);
            Logger.logMessage("Payment [" + txid + "] from peerA " + lockInfo.peerA + " to peerB " + lockInfo.peerB + ". Amount = "
                + newPayment / Constants.ONE_NXT + " NXT");
            continue;
          }
  
          if (lockStatus == EscrowLockStatus.UNLOCKED_B)
          {
            EscrowMessage message = lockInfo.requestIds.get(0);  
            if (message == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }
            
            if (message.row == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }
  
            JSONObject messageValue = message.row.parsedMessage;
            if (messageValue == null)
            {
              Logger.logWarningMessage("Cannot read lock message");
              continue;
            }
  
            JSONObject unlockTransaction = null;
            if(lockInfo.unlockMessage != null && lockInfo.unlockMessage.row != null)
            {
              JSONObject unlockMessageValue = lockInfo.unlockMessage.row.parsedMessage;
              if (unlockMessageValue != null)
              {
                unlockTransaction = (JSONObject) unlockMessageValue.get("transaction");
              }
            }
            
            boolean isAsset = false;
            
            String assetId = TransactionUtils.getAssetsId(message.row.transaction);
            Long assetNQT = TransactionUtils.getAssetsNQT(message.row.transaction);
            if(assetId != null && assetNQT > 0)
            {
              isAsset = true;
            }
            
            //extract encrypted message and send it as a separate attachment
            EncryptedData encData = null;
            try
            {
              EncryptedMessage encryptedMessage = Appendix.EncryptedMessage.parse(unlockTransaction);
              if(encryptedMessage != null)
              {
                encData = encryptedMessage.getEncryptedData();
              }
              unlockTransaction.remove("encryptedMessage");
            }
            catch(Exception e)
            {
            }
            
            JSONObject payment = new JSONObject();
            payment.put("concorde", Constants.CONCORDE_REQUEST_VERSION);
            payment.put("pay", 1L);
            payment.put("lock_id", lockInfo.lockTxId);
            JSON.putNonNull(payment, "transaction", unlockTransaction);
            
            if(isAsset)
            {
              Long newAssetNQT = assetNQT;
              EscrowStateAssetFee assetFee = lockInfo.lockState.assetFees.get(assetId);
              
              if(assetFee == null) continue;
              Long fee = calculateAssetFee(assetId, assetNQT, assetFee);
              newAssetNQT -= fee;
              if(newAssetNQT <= 0)
                continue;
    
              String txid = api.assetTransferSafe(lockInfo.peerA, secret, payment.toString(), encData, assetId, newAssetNQT);
              Logger.logMessage("Payment assets refund [" + txid + "] from peerB " + 
                  lockInfo.peerB + " to peerA " + lockInfo.peerA +
                  ". Asset = " + assetId +
                  ". Amount = " + newAssetNQT);
              continue;
            }
  
            Long newPayment = lockInfo.paymentNQT;
            Long fee = (long) (newPayment * lockInfo.lockState.escrowFee);
            if (fee < lockInfo.lockState.minEscrowFee)
              fee = (long) lockInfo.lockState.minEscrowFee;
  
            newPayment -= fee;
  
            String txid = api.pay(lockInfo.peerA, secret, newPayment, payment.toString(), encData);
            Logger.logMessage("Payment refund [" + txid + "] from peerB " +
                lockInfo.peerB + " to peerA " + lockInfo.peerA + ". Amount = " +
                newPayment / Constants.ONE_NXT + " NXT");
            continue;
          }
        }
        
        running = true;
      }

      //erase previous data. We have too low confirmations filter so transactions could be changed
      db = new TimedState(account, f);
      Logger.logMessage("Block #" + blocks + ". Checking requests... Done.");
      previousRunBlocks = blocks;
    }
  }

  public boolean isBlockchainReady() throws NxtException
  {
    JSONObject status = api.getBlockchainStatus();
    if (status == null)
      return false;

    Long blocks = Convert.nullToZero((Long) status.get("numberOfBlocks"));
    Long blocksFeed = Convert.nullToZero((Long) status.get("lastBlockchainFeederHeight"));
    Boolean isScanning = (Boolean) status.get("isScanning");
    if (isScanning == null)
    {
      isScanning = false;
    }

    if (isScanning)
    {
      return false;
    }

    if (blocks >= blocksFeed)
    {
      return true;
    }

    return false;
  }

  //scan all requests for the account and create a set of locks
  private void scanRequests(String account, String secret) throws NxtException
  {
    try
    {
      db.Update();
      for(TimedRow a : db.database)
      {
        requestProcess(account, secret, a);
      }
    }
    catch (NxtApiException e)
    {
      Logger.logWarningMessage("Exception in scanning requests. " + e.getMessage());
    }
    catch(Exception e)
    {
      throw new NotValidException(e.getMessage());
    }
  }

  private String requestProcess(String account, String secret, TimedRow row)
  {
    if (row.parsedMessage == null) return null;
    if (row.transaction == null) return null;

    String recipient = Convert.emptyToNull((String)row.transaction.get("recipientRS"));
    if(recipient == null) return null;
    String sender = Convert.emptyToNull((String)row.transaction.get("senderRS"));
    if(sender == null) return null;
    
    boolean isAsset = false;
    
    String assetId = TransactionUtils.getAssetsId(row.transaction);
    Long assetNQT = TransactionUtils.getAssetsNQT(row.transaction);
    if(assetId != null && assetNQT > 0)
    {
      isAsset = true;
    }
    
    try
    {
      // got LOCK message
      if (recipient.equals(account) && Convert.nullToZero((Long) row.parsedMessage.get("lock")) == 1L)
      {
        if(isAsset)
        {
          return requestAssetLock(recipient, sender, assetId, assetNQT, row);
        }
        return requestLock(recipient, sender, row);
      }

      // got FREEZE message
      if (recipient.equals(account) && Convert.nullToZero((Long) row.parsedMessage.get("freeze")) == 1L)
      {
        return requestFreeze(recipient, sender, row);
      }

      // got UNLOCK message
      if (recipient.equals(account) && Convert.nullToZero((Long) row.parsedMessage.get("unlock")) == 1L)
      {
        return requestUnlock(recipient, sender, row);
      }

      // got ROLLBACK message
      if (recipient.equals(account) && Convert.nullToZero((Long) row.parsedMessage.get("rollback")) == 1L)
      {
        return requestRollback(recipient, sender, row);
      }

      // PAYMENT
      if (sender.equals(account) && Convert.nullToZero((Long) row.parsedMessage.get("pay")) == 1L)
      {
        return requestPayment(recipient, sender, row);
      }
    }
    catch (ClassCastException e)
    {
      Logger.logWarningMessage("Bad transaction [" + row.tx + "]: " + row.message);
    }
    catch (NumberFormatException e)
    {
      Logger.logWarningMessage("Cannot parse transaction [" + row.tx + "]: " + row.message);
    }

    return null;
  }
  
  public List<EscrowLock> getLocks()
  {
    List<EscrowLock> locks = new ArrayList<EscrowLock>();
    
    synchronized (lock)
    {
      for (String key : escrowLocks.keySet())
      {
        EscrowLock lockInfo = escrowLocks.get(key);
        locks.add(new EscrowLock(lockInfo));
      }
    }
    
    return locks;
  }

  private String requestLock(String recipient, String sender, TimedRow row)
  {
    Long payment = TransactionUtils.getPaymentNQT(row.transaction);
    String recipientMessage = Convert.emptyToNull((String) row.parsedMessage.get("recipientRS"));
    if (recipientMessage == null)
    {
      Logger.logWarningMessage("Malformed LOCK request [" + row.tx + "]: peerA = " + sender + ", paymentNQT = "
            + payment + ", message = " + row.message);
      return null;
    }

    Logger.logMessage("LOCK request [" + row.tx + "]: peerA = " + sender + ", paymentNQT = "
          + payment + ", message = " + row.message);

    EscrowState lockState = null;
    try
    {
      lockState = escrowState.getState(row.blockNum - Constants.DELAY_TO_ACTIVATE_ANNOUNCE_BLOCKS);
    }
    catch (NxtApiException e)
    {
      Logger.logWarningMessage("NXT exception while processing tx [" + row.tx + "]: " + e.getMessage());
      return null;
    }
    catch (NotValidException e)
    {
      Logger.logWarningMessage("Exception while processing tx [" + row.tx + "]: " + e.getMessage());
      return null;
    }
    
    if (payment <= lockState.minEscrowFee)
      return null;

    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;
    escrowMessage.type = EscrowMessageType.LOCK;

    EscrowLock newLock = new EscrowLock();

    newLock.isAsset = false;
    newLock.lockTxId = row.tx;
    newLock.peerA = sender;
    newLock.peerB = recipientMessage;
    newLock.paymentNQT = payment;
    newLock.requestIds.add(escrowMessage);
    newLock.lockState = lockState;
    escrowLocks.put(row.tx, newLock);

    return row.tx;
  }
  
  private String requestAssetLock(String recipient, String sender,
      String assetId, Long assetNQT, TimedRow row)
  {    
    String recipientMessage = Convert.emptyToNull((String) row.parsedMessage.get("recipientRS"));
    if (recipientMessage == null)
    {
      Logger.logWarningMessage("Malformed LOCK request [" + row.tx + "]: peerA = " + sender +
          ", assetId = " + assetId +
          ", assetNQT = " + assetNQT + ", message = " + row.message);
      return null;
    }

    Logger.logMessage("LOCK request [" + row.tx + "]: peerA = " + sender +
        ", assetId = " + assetId +
        ", assetNQT = " + assetNQT + ", message = " + row.message);
    
    EscrowState lockState = null;
    try
    {
      lockState = escrowState.getState(row.blockNum - Constants.DELAY_TO_ACTIVATE_ANNOUNCE_BLOCKS);
    }
    catch (NxtApiException e)
    {
      Logger.logWarningMessage("NXT exception while processing tx [" + row.tx + "]: " + e.getMessage());
      return null;
    }
    catch (NotValidException e)
    {
      Logger.logWarningMessage("Exception while processing tx [" + row.tx + "]: " + e.getMessage());
      return null;
    }
    
    if(!lockState.assetFees.containsKey(assetId))
    {
      Logger.logWarningMessage("Not supported asset id in LOCK request [" + row.tx + "]: peerA = " + sender +
          ", assetId = " + assetId +
          ", assetNQT = " + assetNQT + ", message = " + row.message);
      return null;
    }
    
    EscrowStateAssetFee assetFee = lockState.assetFees.get(assetId);
    
    if (assetNQT <= calculateAssetFee(assetId, assetNQT, assetFee))
    {
      Logger.logWarningMessage("Fee not reached in LOCK request [" + row.tx + "]: peerA = " + sender +
          ", assetId = " + assetId +
          ", assetNQT = " + assetNQT + ", message = " + row.message);
      return null;
    }

    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;
    escrowMessage.type = EscrowMessageType.LOCK;

    EscrowLock newLock = new EscrowLock();

    newLock.isAsset = true;
    newLock.lockTxId = row.tx;
    newLock.peerA = sender;
    newLock.peerB = recipientMessage;
    newLock.paymentNQT = 0L;
    newLock.assetId = assetId;
    newLock.assetNQT = assetNQT;
    newLock.requestIds.add(escrowMessage);
    newLock.lockState = lockState;
    escrowLocks.put(row.tx, newLock);

    return row.tx;
  }

  private String requestFreeze(String recipient, String sender, TimedRow row)
  {
    String lockIdValue = Convert.emptyToNull((String) row.parsedMessage.get("lock_id"));

    if (lockIdValue == null)
    {
      Logger.logWarningMessage("Malformed FREEZE request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    Logger.logMessage("FREEZE request [" + row.tx + "]: sender = " + sender + ", lockId = " + lockIdValue);

    EscrowLock lockInfo = escrowLocks.get(lockIdValue);
    if (lockInfo == null)
    {
      Logger.logWarningMessage("Wrong lock_id in FREEZE request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    // add FREEZE command to the list of requests
    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;

    if (lockInfo.peerA.equals(sender))
    {
      escrowMessage.type = EscrowMessageType.FREEZE_A;
    }
    else if (lockInfo.peerB.equals(sender))
    {
      escrowMessage.type = EscrowMessageType.FREEZE_B;
    }
    else
    {
      Logger.logWarningMessage("Wrong sender in FREEZE request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    lockInfo.requestIds.add(escrowMessage);
    return lockIdValue;
  }

  private String requestPayment(String recipient, String sender, TimedRow row)
  {
    boolean isAsset = false;
    
    String lockIdValue = Convert.emptyToNull((String) row.parsedMessage.get("lock_id"));
    if (lockIdValue == null)
    {
      return null;
    }
    
    EscrowLock lockInfo = escrowLocks.get(lockIdValue);
    if (lockInfo == null)
    {
      Logger.logWarningMessage("Wrong lock_id in PAYMENT request [" + row.tx + "]: recipient = " + 
          recipient + ", message = " + row.message);
      return null;
    }
    
    String assetId = TransactionUtils.getAssetsId(row.transaction);
    Long assetNQT = TransactionUtils.getAssetsNQT(row.transaction);
    if(assetId != null && assetNQT > 0)
    {
      isAsset = true;
    }
    
    if(isAsset)
    {
      Long newAssetNQT = assetNQT;
      EscrowStateAssetFee assetFee = lockInfo.lockState.assetFees.get(assetId);
      
      if(assetFee == null) return null;
      Long fee = calculateAssetFee(assetId, assetNQT, assetFee);

      newAssetNQT -= fee;
      if(newAssetNQT < 0) return null;
    }
    else
    {
      Long payment = TransactionUtils.getPaymentNQT(row.transaction);
      Long newPayment = payment;
      Long fee = (long) (newPayment * lockInfo.lockState.escrowFee);
      if (fee < lockInfo.lockState.minEscrowFee)
        fee = (long) lockInfo.lockState.minEscrowFee;

      newPayment -= fee;
      if(newPayment < 0) return null;
    }

    Logger.logMessage("PAYMENT request [" + row.tx + "]: recipient = " + recipient + ", lockId = " + lockIdValue);

    // add PAYMENT command to the list of requests
    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;

    if (lockInfo.peerA.equals(recipient))
    {
      escrowMessage.type = EscrowMessageType.PAYMENT_A;
    }
    else if (lockInfo.peerB.equals(recipient))
    {
      escrowMessage.type = EscrowMessageType.PAYMENT_B;
    }
    else
    {
      Logger.logWarningMessage("Wrong recipient in PAYMENT request [" + row.tx + "]: recipient = " + 
          recipient + ", message = " + row.message);
      return null;
    }

    lockInfo.requestIds.add(escrowMessage);
    return lockIdValue;
  }

  private String requestUnlock(String recipient, String sender, TimedRow row)
  {    
    String lockIdValue = Convert.emptyToNull((String) row.parsedMessage.get("lock_id"));

    if (lockIdValue == null)
    {
      Logger.logWarningMessage("Malformed UNLOCK request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    Logger.logMessage("UNLOCK request [" + row.tx + "]: sender = " + sender + ", lockId = " + lockIdValue);

    EscrowLock lockInfo = escrowLocks.get(lockIdValue);
    if (lockInfo == null)
    {
      Logger.logWarningMessage("Wrong lock_id in UNLOCK request [" + row.tx + "]: sender = " 
          + sender + ", message = " + row.message);
      return null;
    }

    // add UNLOCK command to the list of requests
    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;

    if (lockInfo.peerA.equals(sender))
    {
      escrowMessage.type = EscrowMessageType.UNLOCK_A;
    }
    else if (lockInfo.peerB.equals(sender))
    {
      escrowMessage.type = EscrowMessageType.UNLOCK_B;
    }
    else
    {
      Logger.logWarningMessage("Wrong sender in UNLOCK request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    lockInfo.requestIds.add(escrowMessage);
    return lockIdValue;
  }

  private String requestRollback(String recipient, String sender, TimedRow row)
  {    
    String lockIdValue = Convert.emptyToNull((String) row.parsedMessage.get("lock_id"));
    if (lockIdValue == null)
    {
      Logger.logWarningMessage("Malformed ROLLBACK request [" + row.tx  + "]: sender = " + sender + 
          ", message = " + row.message);
      return null;
    }

    Logger.logMessage("ROLLBACK request [" + row.tx + "]: sender = " + sender + ", lockId = " + lockIdValue);

    EscrowLock lockInfo = escrowLocks.get(lockIdValue);
    if (lockInfo == null)
    {
      Logger.logWarningMessage("Wrong lock_id in ROLLBACK request [" + row.tx + "]: sender = " + 
          sender + ", message = "  + row.message);
      return null;
    }

    if (!lockInfo.peerA.equals(sender))
    {
      Logger.logWarningMessage("Wrong sender in ROLLBACK request [" + row.tx + "]: sender = " + 
          sender + ", message = " + row.message);
      return null;
    }

    // add ROLLBACK command to the list of requests
    EscrowMessage escrowMessage = new EscrowMessage();
    escrowMessage.row = row;
    escrowMessage.type = EscrowMessageType.ROLLBACK;

    lockInfo.requestIds.add(escrowMessage);
    return lockIdValue;
  }
  
  public static Long calculateAssetFee(String assetId, Long assetNQT, EscrowStateAssetFee fee)
  {
    double calcFee = assetNQT * fee.fee;
    if(calcFee < fee.minimumFee) calcFee = fee.minimumFee;
    return Math.round(Math.ceil(calcFee));
  }
}
