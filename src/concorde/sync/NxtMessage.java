package concorde.sync;

public class NxtMessage
{
  public String sender;
  public String recepient;
  public Long payment;  
  public String message;
  public Long timestamp;
  public String txid;
  public Long height;
}
