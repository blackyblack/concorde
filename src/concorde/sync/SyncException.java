package concorde.sync;

public class SyncException extends Exception {

    /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public SyncException() {
        super();
    }

  public SyncException(String message) {
        super(message);
    }

  public SyncException(String message, Throwable cause) {
        super(message, cause);
    }

  public SyncException(Throwable cause) {
        super(cause);
    }
}

