package concorde.sync;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.json.simple.JSONObject;

import concorde.util.Convert;

public class SyncTransaction
{
  //all messages with access by txid
  private static ConcurrentMap<String, NxtMessage> messages = new ConcurrentHashMap<String, NxtMessage>();
 
  /*
   * Read cached message or download from NRS is not in cache
   */
  public static NxtMessage readMessage(String id, Boolean cache)
      throws SyncException
  {
    if (messages.containsKey(id))
    {
      return messages.get(id);
    }

    // new transaction here
    JSONObject item = null;
    try
    {
      item = SyncApiContainer.api.getTransaction(id);
    } 
    catch (Exception e)
    {
      throw new SyncException("Could not get transaction: " + e.getMessage());
    }

    NxtMessage message = null;
    try
    {
      if (item == null)
        return null;

      JSONObject attach = (JSONObject) item.get("attachment");
      if (attach == null)
        return null;

      message = new NxtMessage();
      message.message = Convert.emptyToNull((String) attach.get("message"));
      message.sender = (String) item.get("senderRS");
      message.recepient = Convert.emptyToNull((String) item.get("recipientRS"));
      String payment = Convert.emptyToNull((String) item.get("amountNQT"));
      try
      {
        message.payment = Long.parseLong(payment);
      } catch (RuntimeException e)
      {
        message.payment = 0L;
      }
      message.txid = id;
      message.timestamp = (Long) item.get("timestamp");
      message.height = (Long) item.get("height");
      // cache it
      if (cache)
      {
        messages.put(id, message);
      }

      return message;
    }
    catch (ClassCastException e)
    {
      return null;
    }
  }
  
  public static void writeMessageToCache(NxtMessage message)
  {
    messages.put(message.txid, message);
  }
  
  public static void cleanCache()
  {
    messages.clear();
  }
}
