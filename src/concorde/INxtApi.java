package concorde;

import java.util.List;

import org.json.simple.JSONObject;

import concorde.NxtException.NxtApiException;
import concorde.crypto.EncryptedData;

public interface INxtApi
{
  public Long now();
  
  public String pay(String recipient, String secretPhrase, Long amount,
      String message, String messageEncrypt) throws NxtApiException;
  
  public String tell(String recipient, String secretPhrase,
      String message, String messageEncrypt) throws NxtApiException;
  
  public String pay(String recipient, String secretPhrase, Long amount,
      String message, EncryptedData encrypted) throws NxtApiException;

  public String tell(String recipient, String secretPhrase,
      String message, EncryptedData encrypted) throws NxtApiException;

  public String readEncryptedMessage(String txid, String secretPhrase) throws NxtApiException;
  
  public List<String> getTransactionIds(String account, Long typeFilter, int timelimit) throws Exception;
  
  public List<JSONObject> getTransactions(String account, Long typeFilter, int timelimit) throws Exception;
  
  public JSONObject getTransaction(String txid) throws Exception;
  
  public String transactionSafe(String recipient, String secretPhrase,
      String message, String messageEncrypt, long amountNQT) throws NxtApiException;
  
  public String assetTransferSafe(String recipient, String secretPhrase,
      String message, String messageEncrypt, String assetId, Long assetNQT) throws NxtApiException;
  
  public JSONObject getBlockchainStatus() throws NxtApiException;
}
