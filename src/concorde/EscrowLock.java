package concorde;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;

import concorde.EscrowMessage.EscrowMessageType;
import concorde.state.EscrowState;
import concorde.util.Convert;
import concorde.util.Logger;

public class EscrowLock
{
  public enum EscrowLockStatus
  {
    NONE, LOCKED, UNLOCKED_A, UNLOCKED_B, FROZEN_A, FROZEN_B, FROZEN, PAYED_A, PAYED_B, REWARD
  }

  public NxtApi api = new NxtApi();
  public List<EscrowMessage> requestIds = new ArrayList<EscrowMessage>();
  public EscrowMessage unlockMessage = null;
  public Long lastIndex = 0L;

  public String lockTxId = null;
  public String peerA = null;
  public String peerB = null;
  public Long paymentNQT = 0L;
  public Long lockHeight = 0L;
  public boolean isAsset = false;
  public Long assetNQT = 0L;
  public String assetId = null;
  
  //the state of the escrow service on the lock block
  public EscrowState lockState = null; 

  public Long lockTimeout = 0L;
  public Long rollbackTimeout = 0L;
  public Long freezeTimeout = 0L;

  public EscrowLockStatus knownStatus = EscrowLockStatus.NONE;

  public EscrowLock()
  {
    requestIds = new ArrayList<EscrowMessage>();
    lastIndex = 0L;
    lockTxId = null;
    peerA = null;
    peerB = null;
    paymentNQT = 0L;
    lockHeight = 0L;
    isAsset = false;
    assetNQT = 0L;
    assetId = null;
    lockState = null;

    lockTimeout = 0L;
    rollbackTimeout = 0L;
    freezeTimeout = 0L;

    knownStatus = EscrowLockStatus.NONE;
  }

  public EscrowLock(EscrowLock lockInfo)
  {
    for (EscrowMessage a : requestIds)
    {
      lockInfo.requestIds.add(a);
    }
    lastIndex = lockInfo.lastIndex;
    lockTxId = lockInfo.lockTxId;
    peerA = lockInfo.peerA;
    peerB = lockInfo.peerB;
    paymentNQT = lockInfo.paymentNQT;
    lockHeight = lockInfo.lockHeight;
    isAsset = lockInfo.isAsset;
    assetNQT = lockInfo.assetNQT;
    assetId = lockInfo.assetId;
    lockState = lockInfo.lockState;

    lockTimeout = lockInfo.lockTimeout;
    rollbackTimeout = lockInfo.rollbackTimeout;
    freezeTimeout = lockInfo.freezeTimeout;

    knownStatus = lockInfo.knownStatus;
  }

  // Update the lock status from the blockchain. Very hard task.
  public EscrowLockStatus updateStatus(Long blockNum)
  {
    EscrowLockStatus status = EscrowLockStatus.NONE;
    knownStatus = EscrowLockStatus.NONE;

    for (EscrowMessage a : requestIds)
    {
      JSONObject transaction = a.row.transaction;
      if (transaction == null) continue;

      try
      {
        Long messageBlock = Convert.nullToZero((Long) transaction.get("height"));
        if (messageBlock == 0L)
        {
          Logger.logMessage("updateStatus failed: height == 0");
          return EscrowLockStatus.NONE;
        }

        Long confirmations = Convert.nullToZero((Long) transaction.get("confirmations"));

        JSONObject json = a.row.parsedMessage;
        if (json == null)
        {
          Logger.logMessage("updateStatus failed: json == null");
          return EscrowLockStatus.NONE;
        }

        // stop immediately on any payment
        if (a.type == EscrowMessageType.PAYMENT_A)
        {
          Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_A");
          status = EscrowLockStatus.PAYED_A;
          continue;
        }

        if (a.type == EscrowMessageType.PAYMENT_B)
        {
          Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_B");
          status = EscrowLockStatus.PAYED_B;
          continue;
        }

        if (status == EscrowLockStatus.NONE)
        {
          if (a.type != EscrowMessageType.LOCK) continue;
          if (confirmations < EscrowMain.requestConfirmations) continue;

          Long lockTimeoutValue = Convert.nullToZero((Long) json.get("lock_timeout"));
          Long rollbackTimeoutValue = Convert.nullToZero((Long) json.get("rollback_timeout"));

          if ((lockTimeoutValue > lockState.maxLockTimeoutBlocks) || (lockTimeoutValue == 0L))
          {
            lockTimeoutValue = lockState.maxLockTimeoutBlocks;
          }

          if ((rollbackTimeoutValue > lockState.maxLockTimeoutBlocks) || (rollbackTimeoutValue == 0L))
          {
            rollbackTimeoutValue = lockState.maxLockTimeoutBlocks;
          }

          lockHeight = messageBlock;
          lockTimeout = lockTimeoutValue + messageBlock;
          rollbackTimeout = rollbackTimeoutValue + messageBlock;

          Logger.logMessage("Lock " + lockTxId + " status updated to LOCKED");
          status = EscrowLockStatus.LOCKED;
          continue;
        }

        if (status == EscrowLockStatus.LOCKED)
        {
          Set<EscrowMessageType> allowedTypes = new HashSet<EscrowMessageType>();
          allowedTypes.add(EscrowMessageType.UNLOCK_A);
          allowedTypes.add(EscrowMessageType.UNLOCK_B);
          allowedTypes.add(EscrowMessageType.FREEZE_A);
          allowedTypes.add(EscrowMessageType.FREEZE_B);
          allowedTypes.add(EscrowMessageType.ROLLBACK);
          // payment may come without request cause of timeout
          allowedTypes.add(EscrowMessageType.PAYMENT_B);

          if (!allowedTypes.contains(a.type)) continue;
          if (confirmations < EscrowMain.requestConfirmations) continue;

          if (a.type == EscrowMessageType.UNLOCK_A)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_A");
            status = EscrowLockStatus.UNLOCKED_A;
            unlockMessage = a;
            continue;
          }

          if (a.type == EscrowMessageType.UNLOCK_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
            status = EscrowLockStatus.UNLOCKED_B;
            unlockMessage = a;
            continue;
          }

          if (a.type == EscrowMessageType.FREEZE_A)
          {
            lockTimeout = lockState.maxFreezeTimeoutBlocks + messageBlock;

            Logger.logMessage("Lock " + lockTxId + " status updated to FROZEN_A");
            status = EscrowLockStatus.FROZEN_A;
            continue;
          }

          if (a.type == EscrowMessageType.FREEZE_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to FROZEN_B");
            status = EscrowLockStatus.FROZEN_B;
            continue;
          }

          if (a.type == EscrowMessageType.ROLLBACK && lockState.rollbackSupport && (messageBlock >= rollbackTimeout))
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
            status = EscrowLockStatus.UNLOCKED_B;
            continue;
          }

          if (a.type == EscrowMessageType.PAYMENT_B && (messageBlock >= lockTimeout))
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_B");
            status = EscrowLockStatus.PAYED_B;
            continue;
          }

          continue;
        }

        if (status == EscrowLockStatus.FROZEN_A)
        {
          Set<EscrowMessageType> allowedTypes = new HashSet<EscrowMessageType>();
          allowedTypes.add(EscrowMessageType.UNLOCK_A);
          allowedTypes.add(EscrowMessageType.UNLOCK_B);
          allowedTypes.add(EscrowMessageType.FREEZE_B);
          // payment may come without request cause of timeout
          allowedTypes.add(EscrowMessageType.PAYMENT_A);

          if (!allowedTypes.contains(a.type)) continue;
          if (confirmations < EscrowMain.requestConfirmations) continue;

          if (a.type == EscrowMessageType.UNLOCK_A)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_A");
            status = EscrowLockStatus.UNLOCKED_A;
            continue;
          }

          if (a.type == EscrowMessageType.UNLOCK_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
            status = EscrowLockStatus.UNLOCKED_B;
            continue;
          }

          if (a.type == EscrowMessageType.FREEZE_B)
          {
            freezeTimeout = lockState.maxFreezeTimeoutBlocks + messageBlock;
            status = EscrowLockStatus.FROZEN;
            Logger.logMessage("Lock " + lockTxId + " status updated to FROZEN");
            continue;
          }

          // FROZEN_A timeout
          if (a.type == EscrowMessageType.PAYMENT_A && (messageBlock >= lockTimeout))
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_A");
            status = EscrowLockStatus.PAYED_A;
            continue;
          }

          continue;
        }

        if (status == EscrowLockStatus.FROZEN_B)
        {
          Set<EscrowMessageType> allowedTypes = new HashSet<EscrowMessageType>();
          allowedTypes.add(EscrowMessageType.UNLOCK_A);
          allowedTypes.add(EscrowMessageType.UNLOCK_B);
          allowedTypes.add(EscrowMessageType.FREEZE_A);
          // payment may come without request cause of timeout
          allowedTypes.add(EscrowMessageType.PAYMENT_B);

          if (!allowedTypes.contains(a.type)) continue;
          if (confirmations < EscrowMain.requestConfirmations) continue;

          if (a.type == EscrowMessageType.UNLOCK_A)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_A");
            status = EscrowLockStatus.UNLOCKED_A;
            continue;
          }

          if (a.type == EscrowMessageType.UNLOCK_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
            status = EscrowLockStatus.UNLOCKED_B;
            continue;
          }

          if (a.type == EscrowMessageType.FREEZE_A)
          {
            freezeTimeout = lockState.maxFreezeTimeoutBlocks + messageBlock;
            status = EscrowLockStatus.FROZEN;
            Logger.logMessage("Lock " + lockTxId + " status updated to FROZEN");
            continue;
          }

          // FROZEN_B timeout
          if (a.type == EscrowMessageType.PAYMENT_B && (messageBlock >= lockTimeout))
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_B");
            status = EscrowLockStatus.PAYED_B;
            continue;
          }

          continue;
        }

        if (status == EscrowLockStatus.FROZEN)
        {
          // do not process requests after freeze timeout
          if (messageBlock >= freezeTimeout) continue;

          Set<EscrowMessageType> allowedTypes = new HashSet<EscrowMessageType>();
          allowedTypes.add(EscrowMessageType.UNLOCK_A);
          allowedTypes.add(EscrowMessageType.UNLOCK_B);

          if (!allowedTypes.contains(a.type)) continue;
          if (confirmations < EscrowMain.requestConfirmations) continue;

          if (a.type == EscrowMessageType.UNLOCK_A)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_A");
            status = EscrowLockStatus.UNLOCKED_A;
            continue;
          }

          if (a.type == EscrowMessageType.UNLOCK_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
            status = EscrowLockStatus.UNLOCKED_B;
            continue;
          }

          continue;
        }

        if (status == EscrowLockStatus.UNLOCKED_A || status == EscrowLockStatus.UNLOCKED_B)
        {
          Set<EscrowMessageType> allowedTypes = new HashSet<EscrowMessageType>();
          allowedTypes.add(EscrowMessageType.PAYMENT_A);
          allowedTypes.add(EscrowMessageType.PAYMENT_B);

          if (!allowedTypes.contains(a.type)) continue;

          if (a.type == EscrowMessageType.PAYMENT_A)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_A");
            status = EscrowLockStatus.PAYED_A;
            continue;
          }

          if (a.type == EscrowMessageType.PAYMENT_B)
          {
            Logger.logMessage("Lock " + lockTxId + " status updated to PAYED_B");
            status = EscrowLockStatus.PAYED_B;
            continue;
          }

          continue;
        }
      }
      catch (Exception e)
      {
        Logger.logMessage("Exception for tx [" + a.row.tx + "]. " + e.getMessage());
        continue;
      }
    }
    
    if (status == EscrowLockStatus.LOCKED)
    {
      if (lockTimeout != 0L && blockNum >= lockTimeout)
      {
        Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_A");
        knownStatus = EscrowLockStatus.UNLOCKED_A;
        return EscrowLockStatus.UNLOCKED_A;
      }
    }

    if (status == EscrowLockStatus.FROZEN_A)
    {
      if (lockTimeout != 0L && blockNum >= lockTimeout)
      {
        Logger.logMessage("Lock " + lockTxId + " status updated to UNLOCKED_B");
        knownStatus = EscrowLockStatus.UNLOCKED_B;
        return EscrowLockStatus.UNLOCKED_B;
      }
    }

    if (status == EscrowLockStatus.FROZEN)
    {
      if (freezeTimeout != 0L && blockNum >= freezeTimeout)
      {
        Logger.logMessage("Lock " + lockTxId + " status updated to REWARD");
        knownStatus = EscrowLockStatus.REWARD;
        return EscrowLockStatus.REWARD;
      }
    }

    knownStatus = status;
    return status;
  }
}
