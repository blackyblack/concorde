package concorde;

import timedstate.TimedRow;

public class EscrowMessage
{
  public enum EscrowMessageType
  {
    NONE,
    LOCK, //lock request (A only allowed)
    FREEZE_A, //freeze request from A
    FREEZE_B, //freeze from B does not really freezes - it confirms FREEZE_A
    UNLOCK_A, //unlock request from A
    UNLOCK_B,
    ROLLBACK, //rollback request (A only allowed)
    PAYMENT_A,  //payment to A
    PAYMENT_B   //payment to B
  }
  
  public EscrowMessageType type = EscrowMessageType.NONE;
  public TimedRow row = null;
}