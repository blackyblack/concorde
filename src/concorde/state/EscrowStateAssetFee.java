package concorde.state;

public class EscrowStateAssetFee
{
  public String assetId;
  public double fee;
  public Long minimumFee;
}
