package concorde.state;

import java.util.HashMap;
import java.util.Map;

import concorde.Constants;

public class EscrowState
{
  public double escrowFee;
  public Long minEscrowFee;
  public Long transactionFee;
  public boolean rollbackSupport;
  public boolean freezeSupport;
  
  public Long maxFreezeTimeoutBlocks;
  public Long maxLockTimeoutBlocks;
  
  public String status;
  
  public Map<String, EscrowStateAssetFee> assetFees;
  
  //fill defaults here
  public EscrowState()
  {
    escrowFee = 0.01;
    minEscrowFee = Constants.ONE_NXT;
    transactionFee = Constants.ONE_NXT;
    
    rollbackSupport = false;
    freezeSupport = true;
    
    maxFreezeTimeoutBlocks = 20000L;
    maxLockTimeoutBlocks = 60000L;
    
    status = "offline";
    
    assetFees = new HashMap<String, EscrowStateAssetFee>();
  }
}
