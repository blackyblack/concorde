package concorde.state;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import timedstate.TimedFilter;
import timedstate.TimedRow;
import timedstate.TimedState;
import concorde.Constants;
import concorde.NxtException.NotValidException;
import concorde.NxtException.NxtApiException;
import concorde.util.Convert;

public class EscrowStateProvider
{
  private TimedState db = null;
  
  public EscrowStateProvider(String account)
  {
    TimedFilter f = new TimedFilter();
    f.dbName = "concorde";
    f.maxDbVersion = Constants.CONCORDE_REQUEST_VERSION;
    f.minDbVersion = Constants.CONCORDE_REQUEST_VERSION;
    f.minConfirmations = 3L;
    f.selectSender = account;
    f.selectRecipient = account;
    f.selectType = 1L;
    
    db = new TimedState(account, f);
  }
  
  //get state for a given block
  public EscrowState getState(Long block) throws NxtApiException, NotValidException
  {
    EscrowState newState = new EscrowState();
    db.Update();
    
    for(TimedRow a : db.database)
    {
      if(a.parsedMessage == null) continue;
      if(a.blockNum > block) break;
      
      if(a.parsedMessage.containsKey("escrowFee"))
      {
        try
        {
          double value = (double)a.parsedMessage.get("escrowFee");
          newState.escrowFee = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("minEscrowFee"))
      {
        try
        {
          Long value = (Long)a.parsedMessage.get("minEscrowFee");
          newState.minEscrowFee = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("transactionFee"))
      {
        try
        {
          Long value = (Long)a.parsedMessage.get("transactionFee");
          newState.transactionFee = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("rollbackSupport"))
      {
        try
        {
          boolean value = (boolean)a.parsedMessage.get("rollbackSupport");
          newState.rollbackSupport = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("freezeSupport"))
      {
        try
        {
          boolean value = (boolean)a.parsedMessage.get("freezeSupport");
          newState.freezeSupport = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("maxFreezeTimeoutBlocks"))
      {
        try
        {
          Long value = (Long)a.parsedMessage.get("maxFreezeTimeoutBlocks");
          newState.maxFreezeTimeoutBlocks = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("maxLockTimeoutBlocks"))
      {
        try
        {
          Long value = (Long)a.parsedMessage.get("maxLockTimeoutBlocks");
          newState.maxLockTimeoutBlocks = value;
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("status"))
      {
        try
        {
          String value = Convert.emptyToNull((String)a.parsedMessage.get("status"));
          if(value == null)
          {
            newState.status = "offline";
          }
          else
          {
            newState.status = value;
          }
        }
        catch(Exception e)
        {
        }
      }
      
      if(a.parsedMessage.containsKey("assetFees"))
      {
        try
        {
          JSONArray value = (JSONArray)a.parsedMessage.get("assetFees");
          if(value != null)
          {
            for(Object o : value)
            {
              JSONObject item = (JSONObject) o;
              String assetId = Convert.emptyToNull((String)item.get("assetId"));
              if(assetId == null) continue;
              
              EscrowStateAssetFee newAssetFee = new EscrowStateAssetFee();
              newAssetFee.assetId = assetId;
              
              try
              {
                newAssetFee.fee = (double)item.get("fee");
                newAssetFee.minimumFee = (Long)item.get("minimumFee");
                
                newState.assetFees.put(assetId, newAssetFee);
              }
              catch(Exception e)
              {
              }
            }
          }
        }
        catch(Exception e)
        {
        }
      }
    }
    
    return newState;
  }
}
