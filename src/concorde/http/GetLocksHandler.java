package concorde.http;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONStreamAware;

import concorde.EscrowLock;
import concorde.EscrowLock.EscrowLockStatus;
import concorde.EscrowMain;


public final class GetLocksHandler extends APITestServlet.APIRequestHandler {
  static final GetLocksHandler instance = new GetLocksHandler();
  
  private GetLocksHandler() {
  }
  
  @SuppressWarnings("unchecked")
  @Override
  JSONStreamAware processRequest(HttpServletRequest req) throws Exception {

    JSONObject answer = new JSONObject();
    
    EscrowMain escrow = EscrowMain.instance;
    
    if (!escrow.isBlockchainReady())
    {
      answer.put("query_status", "bad");
      answer.put("errorCode", 9);
      answer.put("error", "blockchain not ready");
      return answer;
    }
    
    if (!escrow.running)
    {
      answer.put("query_status", "bad");
      answer.put("errorCode", 9);
      answer.put("error", "concorde not ready");
      return answer;
    }
    
    List<EscrowLock> locks = escrow.getLocks();
    JSONArray jsonLocks = new JSONArray();
    for(EscrowLock a : locks)
    {
      JSONObject json = new JSONObject();
      json.put("lock_id", a.lockTxId);
      json.put("lock_height", a.lockHeight);
      json.put("status", a.knownStatus.toString());
      json.put("payment", a.paymentNQT);
      json.put("peer_a", a.peerA);
      json.put("peer_b", a.peerB);
      
      if((a.knownStatus == EscrowLockStatus.LOCKED) ||
         (a.knownStatus == EscrowLockStatus.FROZEN_A) ||
         (a.knownStatus == EscrowLockStatus.FROZEN_B))
      {
        json.put("lock_timeout", a.lockTimeout);
        json.put("rollback_timeout", a.rollbackTimeout);
      }
      
      if(a.knownStatus == EscrowLockStatus.FROZEN)
       {
         json.put("freeze_timeout", a.freezeTimeout);
       }
      
      jsonLocks.add(json);
    }

    answer.put("query_status", "good");
    answer.put("locks", jsonLocks);
    return answer;
  }

  @Override
  boolean requirePost() {
      return true;
  }
}
