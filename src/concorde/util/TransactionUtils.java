package concorde.util;

import org.json.simple.JSONObject;

public class TransactionUtils
{
  public static Long getPaymentNQT(JSONObject transaction)
  {
    String paymentString = Convert.emptyToNull((String) transaction.get("amountNQT"));
    Long payment = 0L;
    try
    {
      payment = Long.parseLong(paymentString);
    }
    catch (RuntimeException e)
    {
    }
    
    return payment;
  }
  
  public static Long getAssetsNQT(JSONObject transaction)
  {
    JSONObject attach = (JSONObject) transaction.get("attachment");
    if(attach == null) return 0L;
    Long version = Convert.nullToZero((Long) attach.get("version.AssetTransfer"));
    if(version < 1) return 0L;
    
    String paymentString = Convert.emptyToNull((String) attach.get("quantityQNT"));
    Long payment = 0L;
    try
    {
      payment = Long.parseLong(paymentString);
    }
    catch (RuntimeException e)
    {
    }
    
    return payment;
  }
  
  public static String getAssetsId(JSONObject transaction)
  {
    JSONObject attach = (JSONObject) transaction.get("attachment");
    if(attach == null) return null;
    Long version = Convert.nullToZero((Long) attach.get("version.AssetTransfer"));
    if(version < 1) return null;
    
    String assetString = Convert.emptyToNull((String) attach.get("asset"));    
    return assetString;
  }
}
