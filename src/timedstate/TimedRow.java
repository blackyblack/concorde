package timedstate;

import org.json.simple.JSONObject;

public class TimedRow
{
  public String tx; //unique key 
  public Long blockNum; //sorted by block
  public String message;
  public JSONObject parsedMessage;
  public JSONObject transaction;
}
