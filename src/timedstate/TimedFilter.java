package timedstate;

public class TimedFilter
{
  //transaction type filter
  public Long selectType;
  
  //transaction direction filter
  public String selectSender;
  public String selectRecipient;
  
  //confirmations filter
  //messages with 1-3 confirmations can disappear. Set to something bigger to create database with persistent data
  public Long minConfirmations;
  
  //filter specific name in rows eg {"concorde":1}
  public String dbName;
  public Long minDbVersion;
  public Long maxDbVersion;
  
  public TimedFilter()
  {
    //no type filter
    selectType = -1L;
    
    //no account filter
    selectSender = null;
    selectRecipient = null;
    
    minConfirmations = 0L;
    
    dbName = null;
    minDbVersion = 0L;
    maxDbVersion = 0L;
  }
}
