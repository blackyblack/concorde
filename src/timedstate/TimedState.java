package timedstate;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import concorde.Constants;
import concorde.NxtApi;
import concorde.NxtException.NotValidException;
import concorde.NxtException.NxtApiException;
import concorde.util.Convert;


public class TimedState
{
  public Set<String> txs = new HashSet<String>();
  public List<TimedRow> database = new ArrayList<TimedRow>();
  public NxtApi api = new NxtApi();
  
  //use this account as database
  public String account = "";
  //blockchain scan filter
  public TimedFilter filter = new TimedFilter();
  //this is the newest tx with 1440 confirmations. Use it to stop searching of new txs
  public String stoptx = "";

  public TimedState(String account, TimedFilter filter)
  {
    this.account = account;
    this.filter = filter;
    
    //clear database and txs
    database = new ArrayList<TimedRow>();
    txs = new HashSet<String>();
    api = new NxtApi();
  }
  
  public void Update() throws NxtApiException, NotValidException
  {    
    List<JSONObject> newtxs = new ArrayList<JSONObject>();
    
    try
    {
      newtxs = api.getTransactions(account, filter.selectType, 0);
    }
    catch (Exception e)
    {
      throw new NxtApiException("Could not get transactions");
    }

    // nothing to sync
    if (newtxs.size() == 0)
    {
      return;
    }
      
    List<TimedRow> newRows = new ArrayList<TimedRow>();
    JSONParser parser = new JSONParser();
    
    boolean first = true;

    // startTime = System.currentTimeMillis();
    // new ids come first
    for(JSONObject item : newtxs)
    {
        String id = null;
        try
        {
          //not empty
          if (item == null) continue;
          
          id = Convert.emptyToNull((String) item.get("transaction"));
          
          if(id == null) continue;
          //stop if this transaction is already processed
          if(id.equals(stoptx)) break;
          //stop if this transaction is already processed
          if(txs.contains(id)) break;
  
          Long confirmations = Convert.nullToZero((Long) item.get("confirmations"));
          
          //here we store the newest permanent tx and do not process txs if stoptx is found
          if(first && confirmations >= Constants.PERMANENT_CONFIRMATIONS_LIMIT)
          {
            stoptx = id;
            first = false;
          }
          
          String sender = Convert.emptyToNull((String) item.get("senderRS"));
          String recipient = Convert.emptyToNull((String) item.get("recipientRS"));
          
          //filter sender and recipient
          if(filter.selectSender != null)
          {
            if(sender == null) continue;
            if(!filter.selectSender.equals(sender)) continue;
          }
          if(filter.selectRecipient != null)
          {
            if(recipient == null) continue;
            if(!filter.selectRecipient.equals(recipient)) continue;
          }
          
          Long block = Convert.zeroToNull((Long) item.get("height"));
          
          //limit by confirmations
          if(confirmations < filter.minConfirmations) continue;
  
          JSONObject attach = (JSONObject) item.get("attachment");
          
          //not empty message
          if (attach == null) continue;
          
          String message = Convert.emptyToNull((String) attach.get("message"));
          //not empty message
          if (message == null) continue;
          
          JSONObject messageValue = null;
          
          try
          {
            messageValue = (JSONObject) parser.parse(message);
          }
          catch (ParseException e)
          {
          }
          catch (NumberFormatException e)
          {
          }
          
          if (messageValue == null) continue;
          
          if(filter.dbName != null && filter.dbName.length() > 0)
          {
            try
            {
              Long dbVersion = Convert.nullToZero((Long) messageValue.get(filter.dbName));
              
              if(dbVersion == 0) continue;
              if(filter.minDbVersion > 0 && dbVersion < filter.minDbVersion) continue;
              if(filter.maxDbVersion > 0 && dbVersion > filter.maxDbVersion) continue;
            }
            catch (ClassCastException e)
            {
              continue;
            }
            catch (NumberFormatException e)
            {
              continue;
            }
            catch (RuntimeException e)
            {
              continue;
            }
          }
          
          TimedRow row = new TimedRow();
          row.blockNum = block;
          row.message = message;
          row.tx = id;
          row.parsedMessage = new JSONObject(messageValue);
          row.transaction = new JSONObject(item);
          
          //add in database
          newRows.add(row);
          txs.add(id);
        }
        catch(Exception e)
        {
          throw new NotValidException("Exception at tx [" + id + "]. " + e.getMessage());
        }
    }
      
    int size = newRows.size();
    for(int i = 0; i < size; i++)
    {
      database.add(newRows.get(size - i - 1));
    }
  }
}
