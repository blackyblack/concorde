# README #

Concorde is an escrow node for NXT coin.
Java 1.7 or higher is required.

# How to use #

Run NRS and wait for blockchain to catch-up.
Open dashboard at http://localhost:17775 to check Concorde status.

Windows:

Run run.bat.

Mac/Linux:

Set execution privileges:

chmod +x run.sh

Run run.sh:

./run.sh